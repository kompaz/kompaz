---
title: "KOMPaz Starthilfe für Miethäuser Projekte in Kiel"
date: 2023-01-21
tags: []
featured_image: ""
description: "KOMPaz ist eine Gruppe als Assoziation von Einzelpersonen, die mit eigenem Projektwunsch oder einfach mit dem Antrieb, andere zu einem Projekt zusammenbringen oder bei ihrem Projekt zu unterstützen und zu fördern."
---
# Wohnraum für alle & keine Profite mit der Miete!

*  14.02. erstes offenes Vernetzungstreffen 
* [Kontakt](KOMPaz@alternativnet.de)

KOMPaz ist eine Gruppe als Assoziation von Einzelpersonen, die mit eigenem Projektwunsch oder einfach mit dem Antrieb, andere zu einem Projekt zusammenbringen oder bei ihrem Projekt zu unterstützen und zu fördern. Ihre Motivation speist sich aus der aktuellen Wohnungsfrage, die als gesamtgesellschaftliche soziale Frage Mieter:innen aktuell unterschiedlich stark herausfordert. KOMPaz möchte zu Lösungen beitragen, die das Bedürfnis nach geschütztem Wohnraum für alle zu sozialen und bezahlbaren Bedingungen zur Kostenmiete in gemeinschaftlicher Selbstorganisierung, Selbstverantwortung und Selbstverwaltung befriedigen.

Doch wie lässt sich Wohnen anders als in hier bekannten Formen der Eigentumsbildung gemeinschaftlich zu organisieren - und nachhaltig gegen den Rückfall in die kapitalistische Profitlogik absichern? Dafür orientiert sich KOMPaz an der Mietshäusersyndikat-Initiative, welche durch ihr innovatives Konzept bereits über 200 Projekte in der BRD hat entstehen lassen.




